//ROS
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h> 
#include "follow_cat/CatCoord.h"
//OPENCV
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/objdetect.hpp"
//Synchronization
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <iostream>
#include <vector>

using namespace std;
using namespace cv;

typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> syncPolicy;

//Publisher
ros::Publisher coord_pub;
 
//Topics
string depth_t="/camera/depth/image_raw";
string rgb_t="/camera/rgb/image_color";

static const float MIN_DISTANCE=0.47;

//Finestre
static const string DEPTH="DEPTH";
static const string RGB="RGB";
static const string CAT="Cat";

//Detection
String cat_cascade_name = "/opt/ros/kinetic/share/OpenCV-3.3.1-dev/haarcascades/haarcascade_frontalcatface_extended.xml";
CascadeClassifier cat_cascade;

bool cat_found= false;

//Tracking
Mat prev_img;
vector<Point2f> features, tracks;

float dist(const Point2f &p1, const Point2f &p2) {
	return sqrt( (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y) );
}

Point centralPt(const vector< Point2f > &pts) {
	int pts_size = pts.size(), x=0, y=0;
	for(int i = 0; i< pts_size; i++){
		x+=cvRound(pts[i].x);
		y+=cvRound(pts[i].y);
	}
	return Point(cvRound(x/pts_size), cvRound(y/pts_size));
}

//Trovo depth facendo media profondità punti intorno
float depth(Point p, Mat &img){
	int x=cvRound(p.x), y=cvRound(p.y), n=0, width=img.cols, height=img.rows;
	float d=0;
	for (int j=y-2; j<y+3; j++){
		for (int i=x-2; i<x+3; i++){
			if (i>=0 && i<width && j>=0 && j<height && img.at<float>(j, i)>0){
				n++;
				d+= img.at<float>(j, i);
			}
		}
	}
	if (n==0) return -1;
	return d/n;
}

void drawPoints( Mat &img, const vector< Point2f > &pts, Scalar color) {
	int pts_size = pts.size(), width =img.cols, height =img.rows;
	for(int i = 0; i< pts_size; i++){
		int xc = cvRound(pts[i].x), yc = cvRound(pts[i].y);
		if ( unsigned(xc) < unsigned(width) && unsigned(yc) < unsigned(height) )
			circle( img, Point(xc,yc),3,color,-1,8 );
	}
}

//Callback attivata quando ricevute immagini rgb e di profondità
void callback (const sensor_msgs::ImageConstPtr& depth_msg,
               const sensor_msgs::ImageConstPtr& rgb_msg){
	
	//Conversione immagini da ROS a OpenCV
	cv_bridge::CvImagePtr cv_rgb;
	cv_bridge::CvImageConstPtr cv_dep;
	try {
		cv_rgb=cv_bridge::toCvCopy(rgb_msg, sensor_msgs::image_encodings::BGR8);
	}catch (cv_bridge::Exception& e){
		ROS_ERROR("cv_bridge exception on rgb image: %s", e.what());
		exit(-1);
	}
	try {
		cv_dep=cv_bridge::toCvShare(depth_msg, sensor_msgs::image_encodings::TYPE_16UC1);
	}catch (cv_bridge::Exception& e){
		ROS_ERROR("cv_bridge exception on depth image: %s", e.what());
		exit(-1);
	}
	
	//Immagine di profondità scalata di 0.001 per avere distanze in metri
	Mat scaled_depth_img;
	(cv_dep->image).convertTo(scaled_depth_img, CV_32F, 0.001);
	
	//Visualizza immagini
	imshow(RGB, cv_rgb->image);
	imshow(DEPTH, scaled_depth_img);
	waitKey(1); //OSS se messo a 0 non si aggiornano le immagini della finestra
	
	//Rielaborazione immagine rgb per detection-tracking
	GaussianBlur(cv_rgb->image, cv_rgb->image, cv::Size(0,0), 1 ); //Rimosso del rumore
	Mat frame_gray;
	cvtColor(cv_rgb->image, frame_gray, COLOR_BGR2GRAY); //Immagine bianco e nero
	equalizeHist( frame_gray, frame_gray); //Aumento contrasto
	
	if (!cat_found){
		/*********** DETECTION ***********/
		vector<Rect> cat_faces;
		
		//Trovati gatti nell'immagine
		cat_cascade.detectMultiScale(frame_gray, cat_faces, 1.1, 4, CV_HAAR_SCALE_IMAGE, Size(80, 80));
		
		int cat_faces_size=cat_faces.size();
		if (cat_faces_size>0){
			
			//Trovato gatto più vicino nell'immagine (rettangolo più grande)
			Rect cat;
			int areaMax=0;
			for (size_t i=0; i<cat_faces_size; i++) {
				if(cat_faces[i].width*cat_faces[i].height > areaMax){
					areaMax = cat_faces[i].width*cat_faces[i].height;
					cat = cat_faces[i];
				}
			}
			float half_width=cat.width/2;
			float half_height=cat.height/2;
			Point center(cat.x + half_width, cat.y + half_height);

			/*********** ESTRAZIONE FEATURES ***********/
      
			//Creata maschera di unsigned char: vale 255 nell'ellisse in cui presente gatto, usato poi per estrarre le features
			Mat mask = Mat::zeros( frame_gray.size(), DataType<uchar>::type );
			ellipse(mask, center, Size(half_width, half_height), 0, 0, 360, Scalar(255), -1);
			
			/* Applicata la maschera manualmente, tuttavia in questo caso sono considerati 
			 * come corner anche i punti dell'estremità della maschera=> usato campo maschera 
			 * della funzione goodFeaturesToTrack
			for( int r = 0; r < frame_gray.rows; ++r) {
				uchar *img_p= frame_gray.ptr<uchar>(r);
				uchar *mask_p= mask.ptr<uchar>(r);
				for( int c = 0; c < frame_gray.cols; ++c, img_p++, mask_p++ ) {
					if (*mask_p > 0) *mask_p= *img_p;
				}
			}
			*/
      
			/*********** Shi and Tomasi corner detector ***********/
      
			double min_distance=2, quality_level=0.05;
			int max_features=100;
			goodFeaturesToTrack (frame_gray, features, max_features, quality_level, min_distance, mask);
		
			if (features.size()>50) {		
				//Disegnate feautures
				Mat cat_detected= (cv_rgb->image).clone();
				ellipse(cat_detected, center, Size(half_width, half_height), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
				drawPoints( cat_detected, features, cv::Scalar(0,0,255) );
				imshow(CAT, cat_detected);
				waitKey(1);
	
				cat_found=true; 
			}
		}
	}
    else { 
		
		//Calcolo baricentro delle features
		Point target=centralPt(features);
		//Calcolo distanza (=media distanze in intorno punto)
		float d=depth(target, scaled_depth_img);
		if (d==-1) d=MIN_DISTANCE*2/3; //oggetto troppo vicino, min distanza misurabile da kinect= circa 0,47 m
		//Mando info=> in altro nodo calcolo distanza-angolo
		follow_cat::CatCoord msg;
		msg.x=cvRound(target.x-(scaled_depth_img.cols/2));
		msg.depth=d;
		coord_pub.publish(msg);
		
		/****** TRACKING ******/
		vector<uchar> status;
		vector<float> errors;
		Size tracking_win_size (3,3);
		calcOpticalFlowPyrLK (prev_img, frame_gray, features, tracks, status, errors, tracking_win_size, 4);
		
		//Elimino tracks che distano cinque volte tanto la deviazione standard corrente (calcolata tramite deviazione media)
		int features_size= int(features.size());
		vector<float> flow_displacement;
		flow_displacement.reserve(features_size);
		double mean = 0, std_dev = 0;
		int num_valid_tracks = 0;
		//Calcolo media
		for (int i = 0; i < features_size; i++){
			float d = 0;
			if (status[i]) { //Solo se è stata calcolata una track per la i-ma feature 
				d = dist(features[i], tracks[i]);
				mean += d;
				num_valid_tracks++;
			}
			flow_displacement.push_back(d);
		}
		if(!num_valid_tracks) num_valid_tracks++;
		mean /= num_valid_tracks;
		
		//Calcolo deviazione standard
		int flow_disp_size=int(flow_displacement.size());
		for (int i = 0; i < flow_disp_size; i++){
			if (status[i])
				std_dev += (flow_displacement[i] - mean)*(flow_displacement[i] - mean);
		}
		std_dev = sqrt(std_dev/num_valid_tracks);
		
		//Selezione features
		features.clear();
		 
		for (int i = 0; i < flow_disp_size; i++){
			 if (status[i] && fabs( flow_displacement[i] - mean) < 5*std_dev)
				features.push_back(tracks[i]);
		}
		tracks.clear();
		
		if (features.size()<20){
			cat_found=false;
			features.clear();
		}
		else {
			//Disegnate feautures
			drawPoints (cv_rgb->image, features, cv::Scalar(0,0,255) );
			imshow(RGB, cv_rgb->image);
			waitKey(1);
		}
	}
    // Aggiornata pagina precedente
    prev_img = frame_gray;
}

int main (int argc, char **argv) {
	
	ros::init(argc, argv, "detection_tracking");
	ros::NodeHandle nh;
	
	//Caricato cascade
	if(!cat_cascade.load(cat_cascade_name)){
		cerr<<"Error while loading HAAR cascades.";
		return -1;
	}
	
	//Creazione finestre per immagini (thread separati)
	namedWindow(RGB);
	startWindowThread();
	namedWindow(DEPTH);
	startWindowThread();
	namedWindow(CAT);
	startWindowThread();
	
	//Publisher coordinate
	coord_pub= nh.advertise<follow_cat::CatCoord>("cat_coord", 100);
 
	//Sottoscrizione sincronizzata ai topic 
	message_filters::Subscriber<sensor_msgs::Image> depth_sub(nh, depth_t, 1);
	message_filters::Subscriber<sensor_msgs::Image> rgb_sub(nh, rgb_t, 1);
	message_filters::Synchronizer<syncPolicy> sync(syncPolicy(10), depth_sub, rgb_sub);
	sync.registerCallback(boost::bind(&callback, _1, _2));
	
	while (ros::ok())
		ros::spinOnce();
	
	//Chiusura finestre
	destroyWindow(RGB);
	destroyWindow(DEPTH);
	destroyWindow(CAT);
	
	return 0;
}
