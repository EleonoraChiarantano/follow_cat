//ROS
#include <ros/ros.h>
#include "follow_cat/CatCoord.h"
#include "geometry_msgs/Twist.h"
#include <math.h>
#include <iostream>

static const int MAX_X=320; // Metà la larghezza dell'immagine
static const double MAX_RAD=0.497; // =28,5°, metà del FOV del kinect
static const double MAX_LIN_VEL=0.6; // m/s
static const double MIN_LIN_VEL=0.02;
static const double MAX_RAD_VEL=2; // rad/s
static const double MIN_RAD_VEL=0.06;
static const double DIST=0.60; // Distanza desiderata dal target

//Publisher
ros::Publisher vel_pub;
//Topic
std::string vel_topic = "cmd_vel";

void cb(const follow_cat::CatCoord::ConstPtr& msg){
	int x=msg->x;
	float d=msg->depth;
	double dist, rad;
	double distCntr=(x*tan(MAX_RAD)*d)/320;
	if(distCntr<0.05 && distCntr>-0.05) {
		rad=0;
		dist=d-DIST;
	}
	else {
		rad=atan2(distCntr, d);
		dist=(distCntr/sin(rad))-DIST;
	}
	
	double lin_vel=MAX_LIN_VEL*dist;
	double ang_vel=MAX_RAD_VEL*rad*-1;
	
	if (lin_vel>MAX_LIN_VEL) lin_vel=MAX_LIN_VEL;
	if (lin_vel<-MAX_LIN_VEL) lin_vel=-MAX_LIN_VEL;
	if (ang_vel>MAX_RAD_VEL) ang_vel=MAX_RAD_VEL;
	if (ang_vel<-MAX_RAD_VEL) ang_vel=-MAX_RAD_VEL;
	if (lin_vel>0 && lin_vel<MIN_LIN_VEL) lin_vel=0;
	if (lin_vel<0 && lin_vel>-MIN_LIN_VEL) lin_vel=0;
	if (ang_vel>0 && ang_vel<MIN_RAD_VEL) ang_vel=0;
	if (ang_vel<0 && ang_vel>-MIN_RAD_VEL) ang_vel=0;
	if (dist<0.05 && dist>-0.05) lin_vel=0;
	
	geometry_msgs::Twist vel_msg;
	vel_msg.linear.x=lin_vel;
	vel_msg.angular.z=ang_vel;
	vel_pub.publish(vel_msg);
}

int main(int argc, char**argv) {
	
	ros::init(argc, argv, "follow");
	ros::NodeHandle nh;

	//Publisher velocità
	vel_pub=nh.advertise<geometry_msgs::Twist>(vel_topic, 100);
	
	//Sottoiscrizione coordinate nodo detection_tracking
	ros::Subscriber sub=nh.subscribe("cat_coord",100,cb);
	
	while (ros::ok())
		ros::spinOnce();
		
	return 0;
}
