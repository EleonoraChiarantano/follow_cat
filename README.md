# follow_cat
Progetto universitario per la laurea triennale in Ingegneria Informatica e Automatica. 
È un package ROS che ha lo scopo di:
* Acquisire immagini dal Microsoft Kinect al fine di fare detection e tracking del viso di un gatto;
* Far inseguire dal robot MARRtino il gatto trovato.

## Prerequisiti
L'applicazione è scritta in C++, usa la libreria OpenCV ed è compilata in ambiente ROS (ros-kinetic). Necessita inoltre di nodi ROS per avviare il robot MARRtino e il Kinect.

Essa richiede quindi l'installazione di [ROS](http://wiki.ros.org/kinetic/Installation) e del software specificato nella [pagina ufficiale di MARRtino](http://sites.google.com/dis.uniroma1.it/marrtino/software).

## Configurazione ed esecuzione

### Configurazione
Una volta clonato il package nella cartella `src` del proprio catkin workspace di ROS (qualora non fosse stato ancora creato, è possibile seguire il seguente [tutorial](http://wiki.ros.org/catkin/Tutorials/create_a_workspace)), basterà ricompilare il proprio workspace aprendo la directory in un terminale ed eseguendo il seguente comando :
```
$ catkin_make
```

### Esecuzione
Su diversi terminali e nell'ordine qui riportato, eseguire i seguenti comandi:
```
cd src/marrtino_apps/robot
roslaunch robot.launch
```
```
roslaunch freenect_launch freenect.launch 
```
```
rosrun follow_cat detection_tracking 
```
```
rosrun follow_cat follow
```

